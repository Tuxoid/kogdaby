
package com.KogdaBy.ByTuxoid.control;

import com.KogdaBy.ByTuxoid.R;
import com.KogdaBy.ByTuxoid.control.model.HTMLParser;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.htmlcleaner.TagNode;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SelectStationFragment extends Fragment {

    private ProgressDialog progressDialog;

    private String stationsURL;

    private StationsAdaper adapterStations;

    private ListView stationsListView;

    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.kontrol_fragment_select_station, null);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        stationsListView = (ListView) view.findViewById(R.id.control_select_station_list_view);
        if (adapterStations != null) {
            stationsListView.setAdapter(adapterStations);
        }
        stationsListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> l, View view, int position, long id) {
                final StationObject s = (StationObject) adapterStations.getItem(position);

                AlertDialog.Builder fireControl = new AlertDialog.Builder(getActivity());

                fireControl.setTitle(getResources().getString(R.string.control_fire_control_title));
                fireControl.setMessage(getResources().getString(R.string.control_fire_control_message)
                        + s.getStationTitle());
                fireControl.setPositiveButton(getResources().getString(R.string.btn_fire_control),
                        new OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                String fireURL = Uri.encode(s.getStationURL(), ALLOWED_URI_CHARS) + "&set=1";
                                FireStationAsyncTask fire = new FireStationAsyncTask();
                                fire.execute(fireURL);
                            }
                        });

                fireControl.setNegativeButton(getResources().getString(R.string.btn_cancel), new OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                fireControl.show();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        if (stationsURL != null) {
            getStationFromWeb();
            stationsURL = null;
        }

    }

    public void loadListForURL(String newUrl) {
        stationsURL = newUrl;
    }

    private void getStationFromWeb() {
        stationsListView.setAdapter(null);
        StationParserAsyncTask parser = new StationParserAsyncTask();
        String urlEncoded = Uri.encode(stationsURL, ALLOWED_URI_CHARS);
        parser.execute(urlEncoded);

    }

    private class FireStationAsyncTask extends AsyncTask<String, Void, HttpResponse> {

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getActivity(), getResources()
                    .getString(R.string.progress_dialog_title),
                    getResources().getString(R.string.control_fire_progress_message));
            progressDialog.setCancelable(false);
        }

        @Override
        protected HttpResponse doInBackground(String... arg) {
            HttpResponse response = null;
            HttpClient client = new DefaultHttpClient();
            HttpGet httpget = new HttpGet();
            try {
                httpget.setURI(new URI(arg[0]));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            try {
                response = client.execute(httpget);
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(HttpResponse response) {
            super.onPostExecute(response);
            progressDialog.dismiss();

            AlertDialog.Builder result = new AlertDialog.Builder(getActivity());
            result.setPositiveButton(getResources().getString(R.string.btn_ok), null);

            if (response.getStatusLine().getStatusCode() == 200) {
                result.setTitle(getResources().getString(R.string.control_success_fire_title));
                result.setMessage(getResources().getString(R.string.control_success_fire_message));
            } else {
                result.setTitle(getResources().getString(R.string.control_fail_fire_title));
                result.setMessage(getResources().getString(R.string.control_fail_fire_message));
            }

            result.show();

        }

    }

    private class StationParserAsyncTask extends AsyncTask<String, Void, List<StationObject>> {

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getActivity(), getResources()
                    .getString(R.string.progress_dialog_title),
                    getResources().getString(R.string.progress_dialog_subtitle_control_load_station));
            progressDialog.setCancelable(false);
        }

        @Override
        protected List<StationObject> doInBackground(String... arg) {
            List<StationObject> output = new ArrayList<StationObject>();
            try {
                HTMLParser hh = new HTMLParser(new URL(arg[0]));
                List<TagNode> links = hh.getLinksByClass("bluelink");

                for (Iterator<TagNode> iterator = links.iterator(); iterator.hasNext();) {
                    TagNode divElement = iterator.next();

                    StationObject newObj = new StationObject(divElement.getText().toString(),
                            divElement.getAttributeByName("href"));
                    output.add(newObj);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return output;
        }

        @Override
        protected void onPostExecute(List<StationObject> output) {
            progressDialog.dismiss();
            adapterStations = new StationsAdaper(getActivity(), output);
            if (stationsListView != null) {
                stationsListView.setAdapter(adapterStations);
            }
        }
    }

    private class StationObject {

        private final String stationTitle;

        private final String stationURL;

        public StationObject(String lTitle, String lURL) {
            this.stationTitle = lTitle;
            this.stationURL = lURL;
        }

        public String getStationTitle() {
            return stationTitle;
        }

        public String getStationURL() {
            return stationURL;
        }

    }

    private class StationsAdaper extends BaseAdapter {

        private final List<StationObject> stations;

        private final Context context;

        private final LayoutInflater inflater;

        public StationsAdaper(Context ctx, List<StationObject> stations) {
            super();
            this.context = ctx;
            this.stations = stations;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return stations.size();
        }

        @Override
        public Object getItem(int position) {
            return stations.get(position);
        }

        @Override
        public long getItemId(int position) {
            return stations.get(position).hashCode();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = inflater.inflate(android.R.layout.simple_list_item_1, null);
            }
            StationObject currLO = (StationObject) getItem(position);
            ((TextView) convertView).setText(currLO.getStationTitle());
            return convertView;
        }
    }

}
