package com.KogdaBy.ByTuxoid.control;

import com.KogdaBy.ByTuxoid.R;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class ControlActivity extends FragmentActivity {

    ControlStartFragment controlStartFragment;

    SelectLetterFragment selectLetterFragment;

    SelectStationFragment selectStationFragment;

    private final String FRAGMENT_START = "FRAGMENT_START";

    private final String FRAGMENT_SELECT_LETTER = "FRAGMENT_SELECT_LETTER";

    private final String FRAGMENT_SELECT_STATION = "FRAGMENT_SELECT_STATION";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.control_activity);

        if (controlStartFragment == null) {
            controlStartFragment = new ControlStartFragment();
        }

        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.fragment_point, controlStartFragment, FRAGMENT_START)
            .commit();

    }

    public void goToSelectLetterFragment() {

        if (selectLetterFragment == null) {
            selectLetterFragment = new SelectLetterFragment();
        }

        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.fragment_point, selectLetterFragment, FRAGMENT_SELECT_LETTER)
            .addToBackStack(null)
            .commit();

    }

	public void goToSelectStationFragment(String letterURL) {
		// TODO Auto-generated method stub
		   if (selectStationFragment == null) {
	            selectStationFragment = new SelectStationFragment();
	        }
           selectStationFragment.loadListForURL(letterURL);
	        getSupportFragmentManager()
	            .beginTransaction()
	            .replace(R.id.fragment_point, selectStationFragment, FRAGMENT_SELECT_STATION)
	            .addToBackStack(null)
	            .commit();

	    }

}
