package com.KogdaBy.ByTuxoid.control;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.KogdaBy.ByTuxoid.R;

public class RaspisanieStartFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.raspisanie_fragment_start, null);

        view.findViewById(R.id.imgBrestRasp).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                
                ((RaspisanieActivity)getActivity()).goToSelectNumberFragment();

            }
        });

        return view;
    }

}
