package com.KogdaBy.ByTuxoid.control;


import com.KogdaBy.ByTuxoid.R;

import com.KogdaBy.ByTuxoid.control.model.HTMLParser;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.htmlcleaner.TagNode;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SelectRaspStationFragment extends Fragment {

    private ProgressDialog progressDialog;

    private String raspStationsURL;

    private RaspStationsAdaper adapterRaspStations;

    private ListView raspStationsListView;

    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.raspisanie_fragment_select_station, null);
        return view;
    }
    

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        raspStationsListView = (ListView) view.findViewById(R.id.raspisanie_select_station_list_view);
        if (adapterRaspStations != null) {
        	raspStationsListView.setAdapter(adapterRaspStations);
        }
        raspStationsListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> l, View view, int position, long id) {
                final RaspStationObject s = (RaspStationObject) adapterRaspStations.getItem(position);

            }
        });
    }


//    @Override
//    public void onViewCreated(View view, Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        raspStationsListView = (ListView) view.findViewById(R.id.raspisanie_select_station_list_view);
//        if (adapterRaspStations != null) {
//        	raspStationsListView.setAdapter(adapterRaspStations);
//        } else {
//            getStationFromWeb();
//        }
//
//        raspStationsListView.setOnItemClickListener(new OnItemClickListener() {
//
//           @Override
//            public void onItemClick(AdapterView<?> l, View view, int position, long id) {
//                ((RaspisanieActivity) getActivity()).goToSelectRaspMarshFragment(((RaspStationObject) adapterRaspStations
//                        .getItem(position)).getRaspStationURL());
//            }
//        });
//    }

    @Override
    public void onStart() {
        super.onStart();
        if (raspStationsURL != null) {
            getStationFromWeb();
            raspStationsURL = null;
        }

    }

    public void loadListForURL(String newUrl) {
        raspStationsURL = newUrl;
    }

    private void getStationFromWeb() {
        raspStationsListView.setAdapter(null);
        StationParserAsyncTask parser = new StationParserAsyncTask();
        String urlEncoded = Uri.encode("http://kogda.by/raspisanie-avtobusov-i-trollejbusov-bresta/" + raspStationsURL, ALLOWED_URI_CHARS);
        parser.execute(urlEncoded);

    }

    private class FireStationAsyncTask extends AsyncTask<String, Void, HttpResponse> {

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getActivity(), getResources()
                    .getString(R.string.progress_dialog_title),
                    getResources().getString(R.string.control_fire_progress_message));
            progressDialog.setCancelable(false);
        }

        @Override
        protected HttpResponse doInBackground(String... arg) {
            HttpResponse response = null;
            HttpClient client = new DefaultHttpClient();
            HttpGet httpget = new HttpGet();
            try {
                httpget.setURI(new URI(arg[0]));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            try {
                response = client.execute(httpget);
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(HttpResponse response) {
            super.onPostExecute(response);
            progressDialog.dismiss();

            AlertDialog.Builder result = new AlertDialog.Builder(getActivity());
            result.setPositiveButton(getResources().getString(R.string.btn_ok), null);

            if (response.getStatusLine().getStatusCode() == 200) {
                result.setTitle(getResources().getString(R.string.control_success_fire_title));
                result.setMessage(getResources().getString(R.string.control_success_fire_message));
            } else {
                result.setTitle(getResources().getString(R.string.control_fail_fire_title));
                result.setMessage(getResources().getString(R.string.control_fail_fire_message));
            }

            result.show();

        }

    }

    private class StationParserAsyncTask extends AsyncTask<String, Void, List<RaspStationObject>> {

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getActivity(), getResources()
                    .getString(R.string.progress_dialog_title),
                    getResources().getString(R.string.progress_dialog_subtitle_control_load_station));
            progressDialog.setCancelable(false);
        }

        @Override
        protected List<RaspStationObject> doInBackground(String... arg) {
            List<RaspStationObject> output = new ArrayList<RaspStationObject>();
            try {
                HTMLParser hh = new HTMLParser(new URL(arg[0]));
                List<TagNode> links = hh.getLinksByClass("bluelink");

                for (Iterator<TagNode> iterator = links.iterator(); iterator.hasNext();) {
                    TagNode divElement = iterator.next();

                    RaspStationObject newObj = new RaspStationObject(divElement.getText().toString(),
                            divElement.getAttributeByName("href"));
                    output.add(newObj);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return output;
        }

        @Override
        protected void onPostExecute(List<RaspStationObject> output) {
            progressDialog.dismiss();
            adapterRaspStations = new RaspStationsAdaper(getActivity(), output);
            if (raspStationsListView != null) {
                raspStationsListView.setAdapter(adapterRaspStations);
            }
        }
    }

    private class RaspStationObject {

        private final String raspStationTitle;

        private final String raspStationURL;

        public RaspStationObject(String lTitle, String lURL) {
            this.raspStationTitle = lTitle;
            this.raspStationURL = lURL;
        }

        public String getRaspStationTitle() {
            return raspStationTitle;
        }

        public String getRaspStationURL() {
            return raspStationURL;
        }

    }

    private class RaspStationsAdaper extends BaseAdapter {

        private final List<RaspStationObject> raspStations;

        private final Context context;

        private final LayoutInflater inflater;

        public RaspStationsAdaper(Context ctx, List<RaspStationObject> raspStations) {
            super();
            this.context = ctx;
            this.raspStations = raspStations;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return raspStations.size();
        }

        @Override
        public Object getItem(int position) {
            return raspStations.get(position);
        }

        @Override
        public long getItemId(int position) {
            return raspStations.get(position).hashCode();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = inflater.inflate(android.R.layout.simple_list_item_1, null);
            }
            RaspStationObject currLO = (RaspStationObject) getItem(position);
            ((TextView) convertView).setText(currLO.getRaspStationTitle());
            return convertView;
        }
    }

}
