package com.KogdaBy.ByTuxoid.control.model;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

public class HTMLParser {
    TagNode rootNode;

    public HTMLParser(URL htmlPage) throws IOException {
        HtmlCleaner cleaner = new HtmlCleaner();
        rootNode = cleaner.clean(htmlPage);
    }

    public List<TagNode> getLinksByClass(String requestedClassname) {
        List<TagNode> linkList = new ArrayList<TagNode>();
        TagNode linkElements[] = rootNode.getElementsByName("a", true);
        for (int i = 0; linkElements != null && i < linkElements.length; i++) {
            String classType = linkElements[i].getAttributeByName("class");
            if (classType != null && classType.equals(requestedClassname)) {
                linkList.add(linkElements[i]);
            }
        }
        return linkList;
    }
}
