package com.KogdaBy.ByTuxoid.control;

import com.KogdaBy.ByTuxoid.R;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class RaspisanieActivity extends FragmentActivity {

    RaspisanieStartFragment raspisanieStartFragment;

    SelectNumberFragment selectNumberFragment;

    SelectRaspStationFragment selectRaspStationFragment;
    
    SelectRaspMarshFragment selectRaspMarshFragment;

    private final String FRAGMENT_START = "FRAGMENT_START"; 

    private final String FRAGMENT_SELECT_NUMBER = "FRAGMENT_SELECT_NUMBER";

    private final String FRAGMENT_SELECT_STATION = "FRAGMENT_SELECT_STATION";
    
    private final String FRAGMENT_SELECT_MARSH = "FRAGMENT_SELECT_MARSH";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.raspisanie_activity);

        if (raspisanieStartFragment == null) {
            raspisanieStartFragment = new RaspisanieStartFragment();
        }

        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.fragment_point2, raspisanieStartFragment, FRAGMENT_START)
            .commit();

    }

    public void goToSelectNumberFragment() {

        if (selectNumberFragment == null) {
            selectNumberFragment = new SelectNumberFragment();
        }

        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.fragment_point2, selectNumberFragment, FRAGMENT_SELECT_NUMBER)
            .addToBackStack(null)
            .commit();

    }

	public void goToSelectRaspStationFragment(String numberURL) {
		// TODO Auto-generated method stub
		   if (selectRaspStationFragment == null) {
	            selectRaspStationFragment = new SelectRaspStationFragment();
	        }
           selectRaspStationFragment.loadListForURL(numberURL);
	        getSupportFragmentManager()
	            .beginTransaction()
	            .replace(R.id.fragment_point2, selectRaspStationFragment, FRAGMENT_SELECT_STATION)
	            .addToBackStack(null)
	            .commit();

	    }
	
	public void goToSelectRaspMarshFragment(String numberURL) {
		// TODO Auto-generated method stub
		   if (selectRaspMarshFragment == null) {
	            selectRaspMarshFragment = new SelectRaspMarshFragment();
	        }
		   selectRaspMarshFragment.loadListForURL(numberURL);
	        getSupportFragmentManager() 
	            .beginTransaction()
	            .replace(R.id.fragment_point2, selectRaspMarshFragment, FRAGMENT_SELECT_MARSH)
	            .addToBackStack(null)
	            .commit();

	    }



}
