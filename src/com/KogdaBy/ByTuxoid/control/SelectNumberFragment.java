package com.KogdaBy.ByTuxoid.control;

import com.KogdaBy.ByTuxoid.R;
import com.KogdaBy.ByTuxoid.control.model.HTMLParser;

import org.htmlcleaner.TagNode;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SelectNumberFragment extends Fragment {

    private ProgressDialog progressDialog;

    private ListView numbersListView;

    private NumbersAdaper adapterNumbers;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.raspisanie_fragment_select_number, null);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        numbersListView = (ListView) view.findViewById(R.id.raspisanie_select_number_list_view);
        if (adapterNumbers != null) {
            numbersListView.setAdapter(adapterNumbers);
        } else {
            getNumbersFromWeb();
        }

        numbersListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> l, View view, int position, long id) {
                ((RaspisanieActivity) getActivity()).goToSelectRaspStationFragment(((NumberObject) adapterNumbers
                        .getItem(position)).getNumberURL());
            }
        });
    }



    private void getNumbersFromWeb() {
        NumberParserAsyncTask parser = new NumberParserAsyncTask();
        // TODO: HARDCODED URL - MOVE FROM HERE!!!
        parser.execute("http://kogda.by/raspisanie-avtobusov-i-trollejbusov-bresta/?mode=vid&town=%D0%91%D1%80%D0%B5%D1%81%D1%82&vid=%D0%90%D0%B2%D1%82%D0%BE%D0%B1%D1%83%D1%81");

    }

    private class NumberParserAsyncTask extends AsyncTask<String, Void, List<NumberObject>> {

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getActivity(), getResources()
                    .getString(R.string.progress_dialog_title),
                    getResources().getString(R.string.progress_dialog_subtitle_control_load_letter));
            progressDialog.setCancelable(false);
        }

        @Override
        protected List<NumberObject> doInBackground(String... arg) {
            List<NumberObject> output = new ArrayList<NumberObject>();
            try {
                HTMLParser hh = new HTMLParser(new URL(arg[0]));
                List<TagNode> links = hh.getLinksByClass("marsh bluelink");

                for (Iterator<TagNode> iterator = links.iterator(); iterator.hasNext();) {
                    TagNode divElement = iterator.next();

                    NumberObject newObj = new NumberObject(divElement.getText().toString(),
                            divElement.getAttributeByName("href"));

                    output.add(newObj);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return output;
        }

        @Override
        protected void onPostExecute(List<NumberObject> output) {
            progressDialog.dismiss();
            adapterNumbers = new NumbersAdaper(getActivity(), output);

            if (numbersListView != null) {
                numbersListView.setAdapter(adapterNumbers);
            }

        }
    }

    private class NumberObject {

        private final String numberTitle;

        private final String numberURL;

        public NumberObject(String lTitle, String lURL) {
            this.numberTitle = lTitle;
            this.numberURL = lURL;
        }

        public String getNumberTitle() {
            return numberTitle;
        }

        public String getNumberURL() {
            return numberURL;
        }

    }

    private class NumbersAdaper extends BaseAdapter {

        private final List<NumberObject> numbers;

        private final Context context;

        private final LayoutInflater inflater;

        public NumbersAdaper(Context ctx, List<NumberObject> numbers) {
            super();
            this.context = ctx;
            this.numbers = numbers;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return numbers.size();
        }

        @Override
        public Object getItem(int position) {
            return numbers.get(position);
        }

        @Override
        public long getItemId(int position) {
            return numbers.get(position).hashCode();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = inflater.inflate(android.R.layout.simple_list_item_1, null);
            }

            NumberObject currLO = (NumberObject) getItem(position);
            ((TextView) convertView).setText(currLO.getNumberTitle());

            return convertView;
        }
    }

}
