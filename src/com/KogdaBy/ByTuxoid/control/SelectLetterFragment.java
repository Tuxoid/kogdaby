
package com.KogdaBy.ByTuxoid.control;

import com.KogdaBy.ByTuxoid.R;
import com.KogdaBy.ByTuxoid.control.model.HTMLParser;

import org.htmlcleaner.TagNode;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SelectLetterFragment extends Fragment {

    private ProgressDialog progressDialog;

    private ListView lettersListView;

    private LettersAdaper adapterLetters;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.kontrol_fragment_select_letter, null);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lettersListView = (ListView) view.findViewById(R.id.control_select_letter_list_view);
        if (adapterLetters != null) {
            lettersListView.setAdapter(adapterLetters);
        } else {
            getLettersFromWeb();
        }

        lettersListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> l, View view, int position, long id) {
                ((ControlActivity) getActivity()).goToSelectStationFragment(((LetterObject) adapterLetters
                        .getItem(position)).getLetterURL());
            }
        });
    }



    private void getLettersFromWeb() {
        LetterParserAsyncTask parser = new LetterParserAsyncTask();
        // TODO: HARDCODED URL - MOVE FROM HERE!!!
        parser.execute("http://kogda.by/kontrolery?mode=control&town=%D0%91%D1%80%D0%B5%D1%81%D1%82");

    }

    private class LetterParserAsyncTask extends AsyncTask<String, Void, List<LetterObject>> {

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getActivity(), getResources()
                    .getString(R.string.progress_dialog_title),
                    getResources().getString(R.string.progress_dialog_subtitle_control_load_letter));
            progressDialog.setCancelable(false);
        }

        @Override
        protected List<LetterObject> doInBackground(String... arg) {
            List<LetterObject> output = new ArrayList<LetterObject>();
            try {
                HTMLParser hh = new HTMLParser(new URL(arg[0]));
                List<TagNode> links = hh.getLinksByClass("bluelink letter");

                for (Iterator<TagNode> iterator = links.iterator(); iterator.hasNext();) {
                    TagNode divElement = iterator.next();

                    LetterObject newObj = new LetterObject(divElement.getText().toString(),
                            divElement.getAttributeByName("href"));

                    output.add(newObj);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return output;
        }

        @Override
        protected void onPostExecute(List<LetterObject> output) {
            progressDialog.dismiss();
            adapterLetters = new LettersAdaper(getActivity(), output);

            if (lettersListView != null) {
                lettersListView.setAdapter(adapterLetters);
            }

        }
    }

    private class LetterObject {

        private final String letterTitle;

        private final String letterURL;

        public LetterObject(String lTitle, String lURL) {
            this.letterTitle = lTitle;
            this.letterURL = lURL;
        }

        public String getLetterTitle() {
            return letterTitle;
        }

        public String getLetterURL() {
            return letterURL;
        }

    }

    private class LettersAdaper extends BaseAdapter {

        private final List<LetterObject> letters;

        private final Context context;

        private final LayoutInflater inflater;

        public LettersAdaper(Context ctx, List<LetterObject> letters) {
            super();
            this.context = ctx;
            this.letters = letters;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return letters.size();
        }

        @Override
        public Object getItem(int position) {
            return letters.get(position);
        }

        @Override
        public long getItemId(int position) {
            return letters.get(position).hashCode();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = inflater.inflate(android.R.layout.simple_list_item_1, null);
            }

            LetterObject currLO = (LetterObject) getItem(position);
            ((TextView) convertView).setText(currLO.getLetterTitle());

            return convertView;
        }
    }

}
