package com.KogdaBy.ByTuxoid;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class TaxiActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.taxi);
        findViewById(R.id.taxi_activity_btn_dial_Apelsin).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) 
			{
            Intent dialAp = new Intent(Intent.ACTION_CALL);
            dialAp.setData(Uri.parse(getResources().getString(R.string.taxi_activity_phone_number)));
            startActivity(dialAp);
            }
        });
    }
}
