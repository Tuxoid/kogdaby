package com.KogdaBy.ByTuxoid;

import com.KogdaBy.ByTuxoid.control.ControlActivity;
import com.KogdaBy.ByTuxoid.control.RaspisanieActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends Activity implements OnClickListener
{
    @Override
    public void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setupListeners();
    }

    private void setupListeners()
	{
        findViewById(R.id.mainButtonContacts).setOnClickListener(this);
        findViewById(R.id.mainButtonContr).setOnClickListener(this);
        
//        Uncomment here when it will be implemented
        findViewById(R.id.mainButtonMaps).setOnClickListener(this);
        findViewById(R.id.mainButtonNews).setOnClickListener(this);
        findViewById(R.id.mainButtonRasp).setOnClickListener(this);
        findViewById(R.id.mainButtonTax).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) 
	{

        Intent intent = new Intent();
        switch (v.getId()) 
		{
        case R.id.mainButtonContacts:
            intent.setClass(this, ContactsActivity.class);
            break;

        case R.id.mainButtonContr:
            intent.setClass(this, ControlActivity.class);
            break;

        case R.id.mainButtonTax:
            intent.setClass(this, TaxiActivity.class);
            break;
            
        case R.id.mainButtonRasp:
            intent.setClass(this, RaspisanieActivity.class);
            break;
        }
		
    if (intent.getComponent() != null && intent.getComponent().getClass() != null) 
    	{
            startActivity(intent);
        }
    }
}
