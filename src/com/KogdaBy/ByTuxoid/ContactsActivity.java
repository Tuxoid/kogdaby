package com.KogdaBy.ByTuxoid;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class ContactsActivity extends Activity
{

    @Override
    public void onCreate(Bundle savedInstanceState)
	{
        super.onCreate(savedInstanceState); 

        // Set contacts.xml as user interface layout
        setContentView(R.layout.contacts);
        findViewById(R.id.contact_activity_mailButtonSend).setOnClickListener(new OnClickListener()
		{

            @Override
            public void onClick(View v) 
			{
                // Sending will be here - another solution for set listener
            }
        });

    }

}
